// 1导入http模块
const http = require('http')
// 2创建web服务器实例
const server = http.createServer()
// 3为服务器实例绑定 request事件，监听客户端的请求
server.on('request',function(req,res){
    const url = req.url
    const method = req.method
    // const str = 'asfasf啊是法证先锋成都市'
    //防止中文乱码，需要设置响应头
    res.setHeader('Content-type','text/html; charset=utf-8')
    let content = '404'
    if(url == '/index.html'){
        res.end('首页')
    }else if(url == '/'){
        res.end('404')
    }
})
// 4  启动服务器
server.listen(80,function(){
    console.log('server running at http://127.0.0.1');
})