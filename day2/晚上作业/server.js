const fs = require('fs')
const http = require('http')
const path = require('path')
let  server = http.createServer((req,res)=>{
    let {url,method} =  req
    console.log(url);
    if(url == '/index' || url == '/'){
        res.setHeader('content-type','text/html;charset=utf8')
        let htmlstr =  fs.readFileSync(path.join(__dirname,'/index.html'),'utf-8');
        res.end(htmlstr)
    }
    else if( path.extname(url)=='.js'){
        let js = fs.readFileSync(path.join(__dirname,url),path.extname(url))
        res.setHeader('content-type','application/javascrip')
        res.end(js)
    }
    else if(url=='/getHeroSkin'&& method =='GET'){
       // res.setHeader('A','application/javascrip')
        let data = fs.readFileSync(path.join(__dirname,'/getHeroSkin.json'),'utf-8')
        res.end(data)
    }
    else{
        res.statusCode = 404
        res.setHeader('content-type','text/html;charset=utf8')
        res.end('404')
    }
})
server.listen(8000,()=>{
    console.log('服务器已开启');
})