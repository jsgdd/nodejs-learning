const http = require('http')
const fs = require('fs')
const path = require('path')
let Server = http.createServer((req,res)=>{
    let urlPath = req.url
    console.log(urlPath);
    if(urlPath=='/' || urlPath == '/index.html'){
        let htmlString = fs.readFileSync(path.join(__dirname,'index.html'),'utf-8')
        res.setHeader('content-type','text/html;charset:utf8')
        res.end(htmlString)
    }
    else if(urlPath == '/style.css'){
        fs.readFile(path.join(__dirname,'style.css'),'utf-8',(err,data)=>{
            res.setHeader('content-type','text/css;charset:utf8')
            res.end(data)
        })
    }
    else if(urlPath == '/111.jpg'){
        fs.readFile(path.join(__dirname,'./image/111.jpg'),'utf-8',(err,data)=>{
            res.setHeader('content-type','image/jpg')
            res.end(data)
        })
       
    }
    else if(urlPath == '/123.js'){
        fs.readFile(path.join(__dirname,'123.js'),'utf-8',(err,data)=>{
            res.setHeader('content-type','application/javascrip')
            res.end(data)
        })
       
    }
    else{
        res.end('404')
            }
        })
  
    

Server.listen(8001,()=>{
    console.log('server running at http://127.0.0.1');
})