const http = require('http')
const fs = require('fs')
const path = require('path')
let server = http.createServer((req,res)=>{
    //获取路径
    let urlPath = req.url
    let content
    if(urlPath === '/index' || urlPath ==='/'){
         content = fs.readFileSync(path.join(__dirname,'index.html'),'utf-8')
        res.setHeader('content-type','text/html;charset:utf8')
    }
    else if(path.extname(urlPath)==='.css'){
        content = fs.readFileSync(path.join(__dirname,urlPath),'utf-8')
        res.setHeader('content-type','text/css;charset:utf8')
    }
    else if(path.extname(urlPath)==='.jpg'){
        content = fs.readFileSync(path.join(__dirname,urlPath))
        res.setHeader('content-type','image/jpg')
    }
    else{
        res.statusCode = 404
        content = fs.readFileSync(path.join(__dirname,'index.html'),'utf-8')
        res.end('资源没有找到')
        return
    }
    res.end(content)
})
server.listen(8000,()=>{
    console.log('服务器已开启');
})