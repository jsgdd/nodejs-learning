const http = require('http');
const fs = require('fs')
const path = require('path');
const server = http.createServer((req,res)=>{
    //http://127.0.0.1:8002/getHeroSkin?name:"后裔"
    let {url,method} = req
    if(method ==='GET' && url.startsWith('/getHeroSkin')){
        // 获取到url字符串中的查询参数
        //1先分割成数组，获取?后面的东西
        let [pathname,query] = url.split('?')
        console.log(query);
        //2使用URLSearchParams解析参数字符串
        let urlSearch = new URLSearchParams(query)
        console.log(urlSearch);
        //使用get这个方法获取后面的属性值
        let namevalue = urlSearch.get('name') //后裔
        console.log(namevalue);
        //使用fs模块获取对应的json
        let dataString = fs.readFileSync(path.join(__dirname,'getHeroSkin.json'),'utf8')
        let datas = JSON.parse(dataString);
        //使用filter找到对应的..
        let filterArr = datas.filter(item => item.name === namevalue);
        res.setHeader('content-type', 'application/json;charset=utf8');
        res.end(JSON.stringify(filterArr))
    }else{
        res.end('404')
    }
})
server.listen(8002,()=>{
    console.log('开启服务器');
})