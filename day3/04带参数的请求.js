const http = require('http')
const fs = require('fs')
const path = require('path')
const server = http.createServer((req,res)=>{
    let {url,method} = req
    //console.log(url,method);
    if(url.startsWith('/getHeroSkin')  && method == 'GET'){
        //console.log(url);
        let [urlname,query] =  url.split("?")
        console.log(query);
        let urlSearch = new URLSearchParams(query)
        console.log(urlSearch);
        let namevalue = urlSearch.get('name')
        console.log(namevalue);
        let jsonstr = fs.readFileSync(path.join(__dirname,'getHeroSkin.json')) 
        let arr = JSON.parse(jsonstr)
        console.log(arr);
        let filterArr = arr.filter((item)=>{
            if(item.name == namevalue){
                return item
            }
        })
        
        res.end(JSON.stringify(filterArr))
    }else{
        url.statusCode =404
        res.end('404')
    }
})
server.listen(8004,()=>{
    console.log('开启8004服务器');
})